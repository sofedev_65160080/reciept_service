/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.dao.ReceiptDao;
import com.werapan.databaseproject.dao.ReceiptDetailDao;
import com.werapan.databaseproject.model.Product;
import com.werapan.databaseproject.model.Receipt;
import com.werapan.databaseproject.model.ReceiptDetail;

import java.util.List;

/**
 *
 * @author ASUS
 */
public class TestReceiptDetailDao {
    public static void main(String[] args) {
        ProductDao pd = new ProductDao();
        ReceiptDetailDao rdd = new ReceiptDetailDao();
        ReceiptDao rd = new ReceiptDao();
        for(ReceiptDetail d : rdd.getAll()) {
            System.out.println(d);
        }
        Receipt receipt1 = rd.get(1);
        List<Product> products =  pd.getAll();
        Product product0 = products.get(0);
        ReceiptDetail rd1 = new ReceiptDetail(product0.getId(),product0.getName(),product0.getPrice(),1,
                product0.getPrice(), receipt1.getId());
        rdd.save(rd1);
    }
}
